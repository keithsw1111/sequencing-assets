### What is this repository for? ###

This repository is a collection of assets useful for christmas light sequencing. While there are lots of these assets available on the internet they are not easy to find. So here a lot are all in one place.

### Contribution guidelines ###

If you have assets you would like to contribute please contact me on http://auschristmaslighting.com ... user keithsw1111